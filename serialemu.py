#!/usr/bin/env python3
#
# serialemu.py: A simple functional partial simulator of a 16550 UART device.
#
# Copyright (c) 2018 John Clemens <clemej1@umbc.edu>
#

import unittest

class serial16550:
        def __init__(self):
                self.regs = { 'txbuf': 0, 'rxbuf': 0, 'dllb': 1, 'ier': 0, 
                        'dlhb': 0, 'iir': 1, 'fcr': 0, 'lcr': 0, 'mcr': 0, 
                        'lsr': 0, 'msr': 0, 'scratch': 0}

                self.reg_read_map = ['rxbuf','ier','iir','lcr','mcr',
                                                        'lsr','msr','scratch']
                self.reg_write_map = ['txbuf','ier','fcr','lcr','mcr',
                                                        None, None, 'scratch']

        def reset(self):
                self.regs = { 'txbuf': 0, 'rxbuf': 0, 'dllb': 1, 'ier': 0, 
                        'dlhb': 0, 'iir': 1, 'fcr': 0, 'lcr': 0, 'mcr': 0, 
                        'lsr': 0, 'msr': 0, 'scratch': 0}

        def baudrate(self):
                if self.regs['dlhb'] == 0 and self.regs['dllb'] == 0:
                        return 1
                return int(115200 / ((self.regs['dlhb']*256) + self.regs['dllb'])) 

        def parity(self):
                if (self.regs['lcr'] & 8) == 0:
                        return 'None'
                if (self.regs['lcr'] & 32) == 0:
                        if (self.regs['lcr'] & 16) == 0:
                                return 'Odd'
                        else:
                                return 'Even'
                if (self.regs['lcr'] & 16) == 0:
                        return 'High'
                else:
                        return 'Low'
        
        def wordlen(self):
                return [5, 6, 7, 8][self.regs['lcr'] & 3]

        def stopbits(self):
                if self.regs['lcr'] & 4 == 0:
                        return 1
                
                if self.wordlen() == 5:
                        return 1.5
                
                return 2

        def is_dlab(self):
                if (self.regs['lcr'] & 128) != 0:
                        return True
                return False

        def status(self):
                return { 'baudrate': self.baudrate(),
                         'wordlen': self.wordlen(),
                         'parity': self.parity(), 
                         'stopbits': self.stopbits(),
                         'txbuf': (hex(self.regs['txbuf']),chr(self.regs['txbuf'])),
                         'rxbuf': (hex(self.regs['rxbuf']),chr(self.regs['rxbuf'])),
                         'scratch': self.regs['scratch'],
                         'regs': [hex(self.read(x)) for x in range(0,8)],
                         'raw_regs': [self.read(x) for x in range(0,8)],
                         'dlab': self.is_dlab() }

        def __repr__(self):
                return str(self.status())

        def read(self, roffset):
                if roffset < 0 or roffset > 7:
                        return

                self.regs['txbuf'] == 0

                if roffset < 2 and self.is_dlab():
                        if roffset == 0: 
                                return self.regs['dllb']
                        return self.regs['dlhb']

                return self.regs[self.reg_read_map[roffset]]

        def write(self, roffset, byte):
                if roffset < 0 or roffset > 7:
                        return

                self.regs['txbuf'] = 0

                if roffset < 2 and self.is_dlab():
                        if roffset == 0: 
                                self.regs['dllb'] = byte
                                return
                        self.regs['dlhb'] = byte
                        return

                if self.reg_write_map[roffset] == None:
                        return

                self.regs[self.reg_write_map[roffset]] = byte
        
        def set_bit(self, roffset, bit):
                if roffset < 0 or roffset > 7 or bit < 0 or bit > 7:
                        return

                self.regs['txbuf'] = 0

                if roffset < 2 and self.is_dlab():
                        if roffset == 0: 
                                self.regs['dllb'] |= 2**bit
                                return
                        self.regs['dlhb'] |= 2**bit
                        return

                if self.reg_write_map[roffset] == None:
                        return

                self.regs[self.reg_write_map[roffset]] |= 2**bit

        def clear_bit(self, roffset, bit):
                if roffset < 0 or roffset > 7 or bit < 0 or bit > 7:
                        return

                self.regs['txbuf'] = 0

                if roffset < 2 and self.is_dlab():
                        if roffset == 0: 
                                self.regs['dllb'] &= ~(2**bit)
                                return
                        self.regs['dlhb'] &= ~(2**bit)
                        return

                if self.reg_write_map[roffset] == None:
                        return

                self.regs[self.reg_write_map[roffset]] &= ~(2**bit)

        def toggle_bit(self, roffset, bit):
                if roffset < 0 or roffset > 7 or bit < 0 or bit > 7:
                        return

                self.regs['txbuf'] = 0

                if roffset < 2 and self.is_dlab():
                        if roffset == 0: 
                                self.regs['dllb'] ^= 2**bit
                                return
                        self.regs['dlhb'] ^= 2**bit
                        return

                if self.reg_write_map[roffset] == None:
                        return

                self.regs[self.reg_write_map[roffset]] ^= 2**bit


class TestSerial16550(unittest.TestCase):
        def setUp(self):
                self.port = serial16550()

        def test_bitopts(self):
                self.port.reset()
                self.port.set_bit(3, 7)
                for r in [7,0,1]:
                        self.port.write(r, 0)
                        self.assertTrue(self.port.read(r) == 0, "Write scratch failed")
                        self.port.set_bit(r, 14)
                        self.assertTrue(self.port.read(r) == 0, "Inv. write not ignored")
                        self.port.clear_bit(r, 14)
                        self.assertTrue(self.port.read(r) == 0, "Inv. write not ignored")
                        self.port.toggle_bit(r, 14)
                        self.assertTrue(self.port.read(r) == 0, "Inv. write not ignored")
                        self.port.set_bit(r, 1)
                        self.assertTrue(self.port.read(r) == 2, "write failed")
                        self.port.set_bit(r, 3)
                        self.assertTrue(self.port.read(r) == 10, "write_failed")
                        self.port.toggle_bit(r, 1)
                        self.assertTrue(self.port.read(r) == 8, "toggle failed")
                        self.port.toggle_bit(r, 1)
                        self.assertTrue(self.port.read(r) == 10, "toggle failed")
                        self.port.clear_bit(r, 0)
                        self.assertTrue(self.port.read(r) == 10, "clear 0 failed")
                        self.port.clear_bit(r, 3) 
                        self.assertTrue(self.port.read(r) == 2, "clear 1 failed")
                
        def test_baudrate(self):
                self.port.reset()
                self.assertTrue(self.port.status()['baudrate'] == 115200)
                self.port.set_bit(3,7)
                self.port.write(0, 2)
                self.assertTrue(self.port.baudrate() == 57600)
                self.port.write(0, 0)
                self.port.write(1, 1)
                self.assertTrue(self.port.baudrate() == 450)
                self.port.write(0, 0xff)
                self.port.write(1, 0xff)
                self.assertTrue(self.port.baudrate() == 1)

        def test_parity(self):
                self.port.reset()
                self.assertTrue(self.port.parity() == 'None')
                self.port.set_bit(3,3)
                self.assertTrue(self.port.parity() == 'Odd')
                self.port.set_bit(3,4)
                self.assertTrue(self.port.parity() == 'Even')
                self.port.set_bit(3,5)
                self.assertTrue(self.port.parity() == 'Low')
                self.port.toggle_bit(3,4)
                self.assertTrue(self.port.parity() == 'High')

if __name__ == '__main__':
        unittest.main()
