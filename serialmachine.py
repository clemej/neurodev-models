#!/usr/bin/env python3
#
# serialmachine.py: Input and output sequence encoding routines
#                   to support an emulated 16550 UART device.  
#
# Copyright (c) 2018 John Clemens <clemej1@umbc.edu>
#

from serialemu import serial16550
import random
import sys
import pickle
import gzip
import numpy as np
import math
import unittest

# 
# Functions to take an arbitrary number and a base, and output
# a floating point vector representation of that number in a 
# specific base, with the fp value scaled between 0 and 1
# 
ovec_sizes = {'float':1, 'hex':5, 'decimal':6, 'binary':17 }
encode_bases = {'float':1, 'hex':16, 'decimal':10, 'binary':2 }

def to_fpvec(num, base, max_len=0):
        if num < 0: 
                return None 
        div = float(base - 1)
        ret = []
        tmp = num
        while tmp != 0:
                ret.insert(0, tmp % base)
                tmp = tmp // base
        if len(ret) > max_len:
                return None
        if len(ret) < max_len:
                for _ in range(max_len-len(ret)):
                        ret.insert(0,0.0)
        return [x/div for x in ret]

def from_fpvec(fpvec, base):
        #clip to between 1 and 0 inclusive, multiply by base, round to nearest 
        #number in base. 
        fpvec = [(float(base-1)*(max(0.0, min(1.0, x)))) for x in fpvec]
        ret = 0
        for f in fpvec:
                ret = base * ret + round(f)
        return int(ret)

#
# fpvec unit tests
#
class TestFpVec(unittest.TestCase):
        def test_fpvec_binary(self):
                zero_four = to_fpvec(0, 2, max_len=4)
                self.assertTrue(zero_four == [0.,0.,0.,0.], "zero wrong") 
                sixteen_four = to_fpvec(16, 2, max_len=4)
                self.assertTrue(sixteen_four == None, "overflow wrong")
                for i in range(65536):
                        vec = to_fpvec(i, 2, max_len=16)
                        self.assertTrue(len(vec) == 16, "wrong size")
                        if i == 255:
                                self.assertTrue(vec == 
                                        [0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,1.,1.,1.,1.,1.], "255 is wrong")
                        test = from_fpvec(vec, 2)
                        self.assertTrue(test == i, "wrong value")

        def test_fpvec_hex(self):
                zero_four = to_fpvec(0, 16, max_len=4)
                self.assertTrue(zero_four == [0.,0.,0.,0.], "zero wrong") 
                sixteen_four = to_fpvec(65539, 16, max_len=4)
                self.assertTrue(sixteen_four == None, "overflow wrong")
                for i in range(65536):
                        vec = to_fpvec(i, 16, max_len=16)
                        self.assertTrue(len(vec) == 16, "wrong size")
                        test = from_fpvec(vec, 16)
                        self.assertTrue(test == i, "wrong value")


        def test_fpvec_dec(self):
                zero_four = to_fpvec(0, 10, max_len=4)
                self.assertTrue(zero_four == [0.,0.,0.,0.], "zero wrong") 
                sixteen_four = to_fpvec(10001, 10, max_len=4)
                self.assertTrue(sixteen_four == None, "overflow wrong")
                for i in range(65536):
                        vec = to_fpvec(i, 10, max_len=16)
                        self.assertTrue(len(vec) == 16, "wrong size")
                        test = from_fpvec(vec, 10)
                        self.assertTrue(test == i, "wrong value")

                

#
# Program, Command and vector generation and conversion
#
def gen_random_statement():
        inputs = {'cmds': ('read', 'write'), 
                        'regs': tuple(range(0,8)), 
                        'values': tuple(range(0,256)) }
        return (random.choice(inputs['cmds']), 
                                random.choice(inputs['regs']),
                                random.choice(inputs['values']))

def gen_program(proglen):
        return [gen_random_statement() for x in range(0, proglen)]

def cmd_to_ivec(cmd):
        ivec = [0.0]*9
        if cmd[0] == 'write':
                ivec[0] = 1.0
        ivec[1+cmd[1]] = 1.0
        if cmd[0] == 'write':
                ivec = ivec + to_fpvec(cmd[2], 2, max_len=8)
        else:
                ivec = ivec + [0.0]*8
                #tmp = reversed(to_fpvec(cmd[2], 2))
                #for idx,val in enumerate(tmp):
                #        ivec[8+(8-idx)] = val

        return ivec

def status_to_ovec(cmd, status, br_encode):

        ovec = [0.0]*12 #(21+ovec_sizes[br_encode])
        ovec[['None', 'Even', 'Odd', 'High', 'Low'].index(status['parity'])] = 1.0
        ovec[5+[5,6,7,8].index(status['wordlen'])] = 1.0
        ovec[9+[1,1.5,2].index(status['stopbits'])] = 1.0

        if br_encode == 'float':
                ovec.append(float(status['baudrate'])/115200.0)
        else:
                ovec = ovec + to_fpvec(int(status['baudrate']), 
                                encode_bases[br_encode], 
                                max_len = ovec_sizes[br_encode])
                #tmp = reversed(to_fpvec(int(status['baudrate']), encode_bases[br_encode]))
                #for idx,val in enumerate(tmp):
                #        ovec[12+ovec_sizes[br_encode]-idx] = val
        if cmd[0] == 'write' and cmd[1] == 0 and status['dlab'] == False:
                ovec.append(1.0)
                ovec = ovec + to_fpvec(cmd[2], 2, max_len=8)
                #ovec[12+ovec_sizes[br_encode]] = 1.0
                #tmp = reversed(to_fpvec(cmd[2], 2))
                #for idx,val in enumerate(tmp):
                #        ovec[14+ovec_sizes[br_encode]-idx] = val
        else:
                ovec = ovec + [0.0]*9

        return ovec

 
# Convert a program into an input encoding. Input encoding is:
# 1 bit read/write, 8 bits reg onehot, 8 bits value = 17 input nodes. 
# Output encoding is:
# - 5 entry one-hot parity 
# - 4 entry one-hot wordlen
# - 3 entry one-hot stopbits
# - *ovec_sizes entry baudrate (fpvec encoded or float)
# - 1 entry t/f output active
# - 8 entry binary data transmitted
# 
def prog_to_vec(prog, br_encode='float'):
        X = []
        Y = []
        port = serial16550()
        port.reset()
        for cmd in prog:
                ivec = cmd_to_ivec(cmd)                
                X.append(ivec)
                
                if cmd[0] == 'write':
                        port.write(cmd[1], cmd[2])

                status = port.status()
                ovec = status_to_ovec(cmd,status,br_encode)
                Y.append(ovec)
                
        return X, Y

import math
def output_to_state(ovec,br_encode='float'):
        ret = {}
        ret['parity'] = ["None", "Even", "Odd", "High", "Low"][list(ovec[0:5]).index(max(ovec[0:5]))]
        ret['wordlen'] = [5,6,7,8][list(ovec[5:9]).index(max(ovec[5:9]))]
        ret['stopbits'] = [1,1.5,2][list(ovec[9:12]).index(max(ovec[9:12]))]
        if br_encode == 'float':
                ret['baudrate'] = round(ovec[12]*115200.0)
        else:
                ret['baudrate'] = from_fpvec(ovec[12:12+ovec_sizes[br_encode]], encode_bases[br_encode])
        ret['TX'] = True if ovec[12+ovec_sizes[br_encode]] > 0.5 else False 
        if ret['TX'] == True:
                ret['data'] = from_fpvec(ovec[12+ovec_sizes[br_encode]+1:], 2)
        return ret
                        

def gen_dataset(cls, proglen, size, br_encode='float'):
        Xs = []
        Ys = []
        count = 0
        while count < size:
                prog = gen_program(proglen)
                x, y = prog_to_vec(prog, br_encode=br_encode)
                Xs.append(x)
                Ys.append(y)
                count += 1
        Xs = np.array(Xs)
        Ys = np.array(Ys)
        return Xs, Ys

def dataset_generator(cls, proglen, br_encode='float'):
        while True:
                prog = gen_program(proglen)
                x, y = prog_to_vec(prog, br_encode=br_encode)
                yield np.array(x), np.array(y)        

class TestSerialEncoding(unittest.TestCase):
        def test_gen_dataset(self):
                for br_encode in ovec_sizes:
                        x, y = gen_dataset(None, 20, 2000, br_encode=br_encode)
                        i = np.array(x)
                        o = np.array(y)
                        self.assertTrue(i.shape == (2000, 20, 17))
                        self.assertTrue(o.shape == 
                                (2000, 20, 21+ovec_sizes[br_encode]), 
                                        "output failed: %s" % (br_encode,))

        def test_dataset_generator(self):
                for br_encode in ovec_sizes:
                        it = dataset_generator(None, 20, br_encode=br_encode)
                        x,y = next(it)
                        self.assertTrue(x.shape == (20, 17))
                        self.assertTrue(y.shape == 
                                (20, 21+ovec_sizes[br_encode]), 
                                        "output failed: %s" % (br_encode,))

class TestPrograms(unittest.TestCase):
        def setUp(self):
                # Program 1:  115200 is the default. 
                # Set 8,N,1 by writing to reg 3
                self.prog1 = [
                        ('write', 3,0x03), # 8n1
                        ('write', 0,0x48),
                        ('write', 0,0x65),
                        ('write', 0,0x6c),
                        ('write', 0,0x6c),
                        ('write', 0,0x6f),
                        ('write', 0,0x20),
                        ('write', 0,0x57),
                        ('write', 0,0x6f),
                        ('write', 0,0x72),
                        ('write', 0,0x6c),
                        ('write', 0,0x64),
                        ('write', 0,0x21),
                        ('write', 0,0x0a),
                        ('write', 7, 0x00),
                        ('write', 7, 0x00), 
                        ('write', 7, 0x00) 
                ]
                
                self.prog2 = [
                        ('write', 3, 0x80), # Set DLAB
                        ('write', 0, 0x0c), # divisor to 9600
                        ('write', 3, 0x00), # Unset DLAB
                        ('write', 3,0x1a), # 7e1
                        ('write', 0,0x48),
                        ('write', 0,0x65),
                        ('write', 0,0x6c),
                        ('write', 0,0x6c),
                        ('write', 0,0x6f),
                        ('write', 0,0x20),
                        ('write', 0,0x57),
                        ('write', 0,0x6f),
                        ('write', 0,0x72),
                        ('write', 0,0x6c),
                        ('write', 0,0x64),
                        ('write', 0,0x21),
                        ('write', 0,0x0a)
                ]

                self.prog3 = [
                        ('write', 3, 0x80), # Set DLAB
                        ('write', 0, 0x30), # divisor to 2400
                        ('write', 3, 0x00), # Unset DLAB
                        ('write', 3,0x0e), # 7o2
                        ('write', 0,0x48),
                        ('write', 0,0x65),
                        ('write', 0,0x6c),
                        ('write', 0,0x6c),
                        ('write', 0,0x6f),
                        ('write', 0,0x20),
                        ('write', 0,0x57),
                        ('write', 0,0x6f),
                        ('write', 0,0x72),
                        ('write', 0,0x6c),
                        ('write', 0,0x64),
                        ('write', 0,0x21),
                        ('write', 0,0x0a)
                ]

        def dict_subset(self, d1, d2):
                for k in d1:
                        if k in d2:
                                if d1[k] != d2[k]:
                                        return False
                return True

        def test_hello_world(self):
                for enc in ovec_sizes:
                        for prog in [self.prog1,self.prog2,self.prog3]:
                                port = serial16550()
                                port.reset()
                                ret = ''
                                for cmd in prog:
                                        ivec = cmd_to_ivec(cmd)
                                        if cmd[0] == 'write':
                                                port.write(cmd[1], cmd[2])
                                        status = port.status()
                                        ovec = status_to_ovec(cmd,status,enc)
                                        stat2 = output_to_state(ovec,enc)
                                        self.assertTrue(self.dict_subset(stat2,status),
                                                "%s : %s" % (status,stat2))
                                        if stat2['TX']:
                                                ret += chr(stat2['data'])
                                self.assertTrue(ret == 'Hello World!\n')


if __name__ == '__main__':
        unittest.main()
