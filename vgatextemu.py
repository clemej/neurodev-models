#!/usr/bin/env python3
#
# vgatextemu.py: A simple software model of VGA text mode.
#                Includes utility functions to output
#                framebuffer in HTML.
#
# Copyright (c) 2018 John Clemens <clemej1@umbc.edu>
#
# Warning: This code/machine is not tested or used as much as the
#          bit and uart machines / emulators
# 


class vgatext:
        def __init__(self, X, Y):
                self.X = X
                self.Y = Y
                self.memory = [0]*(self.X*self.Y*2)
                
                self.vga_color_map = ['black','blue','green','cyan','red',
                                        'magenta','brown','gray','darkgray',
                                        'brightblue','brightgreen','brightcyan',
                                        'brightred','brightmagenta','yellow',
                                        'white']

        def reset(self):
                self.memory = [0]*(self.X*self.Y*2)

        def status(self):
                return { 'rawmem': self.memory,
                         'state': self.cellstate() }

        def get_coloridx(self, color):
                return self.vga_color_map.index(color)

        def __cell_offset(self, x, y):
                return (y*self.X*2)+(x*2)

        def cell(self, x, y):
                ret = {}
                offset = self.__cell_offset(x,y)
                attrs = self.memory[offset]
                char = self.memory[offset+1]
                ret['char'] = char
                if attrs > 127: 
                        ret['blink'] = True
                else:
                        ret['blink'] = False
                attrs = attrs & 0x7f
                ret['bgcolor'] = self.vga_color_map[attrs >> 4]
                ret['fgcolor'] = self.vga_color_map[attrs & 0x0f]

                return ret
                

        def cellstate(self):
                # Return an 80x25 array of json encoded data about each cell.
                #   { blink: False, fgcolor: "str", bgcolor: "str", char: 0-255 }
                # All encodings are in IBM cp437
                ret = []
                for y in range(self.Y):
                        for x in range(self.X):
                                ret.append(self.cell(x,y))

                return ret
                

        def __repr__(self):
                return str(self.status())

        def read(self, offset, length):
                if offset < 0 or offset+length > (self.X*self.Y*2):
                        return None

                self.memory[offset:offset+length]

        def write(self, offset, data):
                if offset < 0 or offset+len(data) > (self.X*self.Y*2):
                        return
                
                self.memory[offset:offset+len(data)] = data             


# 
# A giant lookup table to translate CP437 numbers to unicode hex. 
#

def translate_cp437(char):
        # Table taken from wikipedia cp437 page
        if char > 0x1F and char < 0x7F:
                return char

        table = {
                0   : 0x0000,
                1   : 0x263a,
                2   : 0x263b,
                3   : 0x2665,
                4   : 0x2666, 
                5   : 0x2663,
                6   : 0x2660, 
                7   : 0x2022,
                8   : 0x25d8,
                9   : 0x25cb,
                10  : 0x25d9,
                11  : 0x2642,
                12  : 0x2640,
                13  : 0x266a,
                14  : 0x266b,
                15  : 0x263c,
                16  : 0x25ba,
                17  : 0x25c4,
                18  : 0x2195,
                19  : 0x203c,
                20  : 0x00b6,
                21  : 0x00a7,
                22  : 0x25ac,
                23  : 0x21a8,
                24  : 0x2191,
                25  : 0x2193,
                26  : 0x2192,
                27  : 0x2190,
                28  : 0x221F,
                29  : 0x2194,
                30  : 0x25b2,
                31  : 0x25bc,
                # Skip ascii range
                127 : 0x2302,
                128 : 0x00c7,
                129 : 0x00fc,
                130 : 0x00e9,
                131 : 0x00e2,
                132 : 0x00e4,
                133 : 0x00e0,
                134 : 0x00e5,
                135 : 0x00e7,
                136 : 0x00ea,
                137 : 0x00eb,
                138 : 0x00e8,
                139 : 0x00ef,
                140 : 0x00ee,
                141 : 0x00ec,
                142 : 0x00c4,
                143 : 0x00c5,
                144 : 0x00c9,
                145 : 0x00e6,
                146 : 0x00c6,
                147 : 0x00f4,
                148 : 0x00f6,
                149 : 0x00f2,
                150 : 0x00fb,
                151 : 0x00f9,
                152 : 0x00ff,
                153 : 0x00d6,
                154 : 0x00dc,
                155 : 0x00a2,
                156 : 0x00a3,
                157 : 0x00a5, 
                158 : 0x20a7,
                159 : 0x0192,
                160 : 0x00e1,
                161 : 0x00ed,
                162 : 0x00f3,
                163 : 0x00fa, 
                164 : 0x00f1,
                165 : 0x00d1,
                166 : 0x00aa,
                167 : 0x00ba,
                168 : 0x00bf,
                169 : 0x2310,
                170 : 0x00ac,
                171 : 0x00bd,
                172 : 0x00bc,
                173 : 0x00a1,
                174 : 0x00ab,
                175 : 0x00bb,
                176 : 0x2591,
                177 : 0x2592,
                178 : 0x2593,
                179 : 0x2502,
                180 : 0x2524,
                181 : 0x2461,
                182 : 0x2562,
                183 : 0x2556,
                184 : 0x2555,
                185 : 0x2563,
                186 : 0x2551,
                187 : 0x2557,
                188 : 0x255d,
                189 : 0x255c,
                190 : 0x255b,
                191 : 0x2510,
                192 : 0x2514,
                193 : 0x2534,
                194 : 0x232c,
                195 : 0x251c,
                196 : 0x2500,
                197 : 0x25ec,
                198 : 0x255e,
                199 : 0x255f,
                200 : 0x255a,
                201 : 0x2554,
                202 : 0x2569,
                203 : 0x2566,
                204 : 0x2560,
                205 : 0x2550,
                206 : 0x256c,
                207 : 0x2567,
                208 : 0x2568,
                209 : 0x2564,
                210 : 0x2565,
                211 : 0x2559,
                212 : 0x2558,
                213 : 0x2552,
                214 : 0x2553,
                215 : 0x256b,
                216 : 0x256a,
                217 : 0x2518,
                218 : 0x250c,
                219 : 0x2588,
                220 : 0x2584,
                221 : 0x258c,
                222 : 0x2590,
                223 : 0x2580,
                224 : 0x03b1,
                225 : 0x00df,
                226 : 0x0393,
                227 : 0x03c0,
                228 : 0x03a3,
                229 : 0x03c3,
                230 : 0x00b5,
                231 : 0x03c4,
                232 : 0x03a6,
                233 : 0x0398,
                234 : 0x03a9,
                235 : 0x03b4,
                236 : 0x221e,
                237 : 0x03c6,
                238 : 0x03b5,
                239 : 0x2229,
                240 : 0x2261,
                241 : 0x00b1,
                242 : 0x2265,
                243 : 0x2264,
                244 : 0x2320,
                245 : 0x2321,
                246 : 0x00f7,
                247 : 0x2248,
                248 : 0x00b0,
                249 : 0x2219,
                250 : 0x00b7,
                251 : 0x221a,
                252 : 0x207f,
                253 : 0x00b2,
                254 : 0x25a0,
                255 : 0x00a0 }

        return table[char]

def ega_to_rgb(color):
        return {'black':'#000000', 'blue': '#0000aa', 'green': '#00aa00',
                'cyan': '#00aaaa', 'red': '#aa0000', 'magenta': '#aa00aa',
                'brown': '#aa5500', 'gray': '#aaaaaa', 'darkgray': '#555555',
                'brightblue': '#5555ff','brightgreen':'#55ff55',
                'brightcyan': '#55ffff','brightred':'#ff5555',
                'brightmagenta': '#ff55ff','yellow': '#ffff55',
                'white': '#ffffff' }[color]


def json_cell_to_html(cell):
        return '<span %s style="color: %s; background-color: %s;">&#x%s;</span>' % \
                ('class="blink"' if cell['blink'] else '', 
                ega_to_rgb(cell['fgcolor']), ega_to_rgb(cell['bgcolor']),
                format(translate_cp437(cell['char']), 'x'))

def json_to_html(X,Y,cells):
        html = ''
        for y in range(Y):
                for x in range(X):
                        html += json_cell_to_html(cells[(y*X)+x])
                html += '<br/>'
        return html

def json_to_html_header():
        html = '<!DOCTYPE html>\n<html>\n'
        html += '''
<head>
<style>
.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
</style>
<body>
<pre>
'''
        return html

def json_to_html_footer():
        return '</pre>\n</body>\n</html>\n'

def json_to_html_doc(X, Y, cells):
        html = json_to_html_header()
        html += json_to_html(X, Y, cells)
        html += json_to_html_footer()
        return html
