Neuro-Device Models
===================

[![pipeline status](https://gitlab.com/clemej/neurodev-models/badges/master/pipeline.svg)](https://gitlab.com/clemej/neurodev-models/commits/master)

Software emulators and models of hardware devices to generate 
neural network training data.  Used to generate the datasets
used in 

 * "Learning Device Models with Recurrent Neural Networks", J. Clemens, IJCNN 2018.  
\[ [paper]() | [slides](http://www.deater.net/john/pdfs/ijcnn-pres.pdf) | [arXiv](https://arxiv.org/abs/1805.07869) | [jupyter](https://gitlab.com/clemej/neurodev-models/tree/master/ijcnn18/ijcnn18-demo.ipynb) | bibtex \]

Usage
-----

Use generate\_training\_data.py as shown below to generate an HDF5 file
with encoded input and output sequences for each of the desired machine
types.  It generates the specified number of training sequences, validation
sequences, and test sequences to be used for training, validation, and 
testing neural networks respectively. 

    Usage: <outfile.hdf5> <seqlen> <trainsize> <valsize> <testsize> <machinename(s)>

For example, the following command will generate an HDF5 file containing
4096 training, 512 validation, and 128 test sequences of length 1024 for
both the XORMachine and SerialPortMachine models:

    python3 ./generate_training_data.py dataset.hdf5 1024 4096 512 128 XORMachine SerialPortMachine

The resulting `dataset.hdf5` will have the following `groups[datasets]`:

    XORMachine[Xt] : Training inputs
    XORMachine[Yt] : Training outputs
    XORMachine[Xv] : Validation inputs
    XORMachine[Yv] : Validation outputs
    XORMachine[Xs] : Test inputs
    XORMachine[Ys] : Test outputs
    SerialPortMachine[Xt] : Training inputs
    SerialPortMachine[Yt] : Training outputs
    SerialPortMachine[Xv] : Validation inputs
    SerialPortMachine[Yv] : Validation outputs
    SerialPortMachine[Xs] : Test inputs
    SerialPortMachine[Ys] : Test outputs


Currently supported machines:
----------------------------

 * **EightBitMachine**: 8 inputs connected to 8 outputs with memory
 * **SingleDirectMachine**: 8 inputs, but only one connected to one output,
                        with memory.
 * **SingleInvertMachine**: Same as above, but single output is inverted. 
 * **XORMachine**: 8 inputs, with two inputs XOR'd to generate a single 
                        output with memory.
 * **ParityMachine**: 8 inputs combine for one output determined by the
                      number of currently set inputs, with memory.
 * **SerialPortMachine**: An simplified 16550 UART (TX only, no interrupts)
 * **VGATextMachine**: A software model that emulates a VGA text mode 
                        framebuffer.



                         
