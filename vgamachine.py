#!/usr/bin/env python3
#
# vgamachine.py: Input and output sequence encoding and wrapper
#                for an emulated VGA text mode device.
#
# Copyright (c) 2018 John Clemens <clemej1@umbc.edu>
#


from vgatextemu import vgatext
import random
import sys
import pickle
import gzip
import numpy as np
import copy

#FB_SIZE = 4000 #80*25*2

def gen_random_statement(vgacls):
        #statements = ('write', 'read') # (ignore read since it can not change state)?
        inputs = {'cmds': ('read', 'write'), 
                        'offset': tuple(range(0,vgacls.X*vgacls.Y*2)), 
                        'values': tuple(range(0,256)) }
        return (random.choice(inputs['cmds']), 
                                random.choice(inputs['offset']),
                                random.choice(inputs['values']))

def gen_program(proglen, cls):
        return [gen_random_statement(cls) for x in range(0, proglen)]

# 
# Convert a program into an input encoding. Input encoding is:
# - 1 bit read/write, 
# - 12 bits offset, 
# - 8 bits value = 12 input nodes. 
# Output encoding is:
#   - XxY instances of
#     - 1 bit blink
#     - 1 float (1.0/fg_color_index)
#     - 1 float (1.0/bg_color_index)
#     #- 16 entry fgcolor (onehot)
#     #- 8 entry bgcolor (onehot)
#     - 8 bit character in cell (cp437)
#
def prog_to_vec(prog, vga):
        X = []
        Y = []
        vga.reset()
        for cmd in prog:
                ivec = [0.0]*21
                if cmd[0] == 'write':
                        ivec[0] = 1.0
                for idx,bit in enumerate(format(cmd[1], "12b")):
                        if bit == '1': ivec[1+idx] = 1.0 
                if cmd[0] == 'write':
                        for idx,bit in enumerate(format(cmd[2], "08b")):
                                if bit == '1': ivec[13+idx] = 1.0
                # same input vec for command.
                X.append(ivec)
                
                # run instruction
                if cmd[0] == 'write':
                        vga.write(cmd[1], [cmd[2]])

                # calculate Y
                ovec = []
                status = vga.status()
                for cell in status['state']:
                        #ent = [0.0]*16
                        #ent = [0.0]*4
                        #ent = [0.0]*33
                        ent = [0.0]*281

                        if cell['blink']:
                                ent[0] = 1.0

                        # 
                        # Use these for tightest/float encoding
                        # Works, but toughest/longest to converge
                        #                       
                        #ent[1] = 1.0 - float(vga.get_coloridx(cell['bgcolor']))/7.0
                        #ent[2] = 1.0 - float(vga.get_coloridx(cell['fgcolor']))/15.0
                        #ent[3] = 1.0 - float(cell['char'])/255.0
                        
                        # 
                        # Use this for one-hot encoding of all attrbiutes. This is HUGE.
                        # but should converge the fastest.
                        # 8-entry bgcolor
                        # 16-entry fgcolor
                        # 256-entry char
                        #
                        ent[1+vga.get_coloridx(cell['bgcolor'])] = 1.0
                        ent[9+vga.get_coloridx(cell['fgcolor'])] = 1.0
                        ent[25+int(cell['char'])] = 1.0
                        # 
                        # Use these for straight-through binary 
                        # encoding. 3-bit bgcolor, 4-bit fgcolor, 8-bit char
                        # Second tightest, but lest interesting. 
                        # 
                        #for idx, bit in enumerate(format(vga.get_coloridx(cell['bgcolor']), "03b")):
                        #        if bit == 1:
                        #                ent[1+idx] = 1.0

                        #for idx, bit in enumerate(format(vga.get_coloridx(cell['fgcolor']), "04b")):
                        #        if bit == 1:
                        #                ent[4+idx] = 1.0

                        #for idx,bit in enumerate(format(cell['char'], "08b")):
                        #       if bit == 1: 
                        #                ent[8+idx] = 1.0
                        
                        ovec += ent

                        
                Y.append(ovec)
                
        return X, Y

def gen_dataset(cls, proglen, size):
        Xs = []
        Ys = []
        count = 0
        while count < size:
                if (count % 16) == 0:
                        print("count = %d" % (count,))
                prog = gen_program(proglen, cls)
                x, y = prog_to_vec(prog, cls)
                Xs.append(x)
                Ys.append(y)
                count += 1
        Xs = np.array(Xs)
        Ys = np.array(Ys)
        return Xs, Ys

def dataset_generator(cls, proglen):
        while True:
                prog = gen_program(proglen, cls)
                x, y = prog_to_vec(prog, cls)
                yield np.array(x), np.array(y)

# Expect a single X*Y size 1D array of floats.
def output_to_json_float(Ypred):
        vga = vgatext(1,1)
        ret = []
        entry = {}
        for idx,val in enumerate(Ypred):
                if idx % 4 == 0:
                        entry = {}
                        entry['blink'] = True if round(val) == 1.0 else False

                if idx % 4 == 1:
                        entry['bgcolor'] = vga.vga_color_map[1-max(min(int(round(val*7.0)), 7), 0)]
                if idx % 4 == 2:
                        entry['fgcolor'] = vga.vga_color_map[1-max(min(int(round(val*15.0)), 15), 0)]
                if idx % 4 == 3:
                        entry['char'] = max(0,min(int(round((1.0-val)*255.0)), 255))
                        ret.append(copy.deepcopy(entry))

        return ret

def output_to_json_binary(Ypred):
        vga = vgatext(1,1)
        ret = []
        def chunker(seq, size):
                return (seq[pos:pos + size] for pos in range(0, len(seq), size))

        for e in chunker(Ypred, 16):
                entry = {}
                entry['blink'] = True if round(e[0]) == 1.0 else False
                entry['bgcolor'] = vga.vga_color_map[int(round(round(e[1])*4 + round(e[2])*2 + round(e[3])))]
                entry['fgcolor'] = vga.vga_color_map[int(round(round(e[4])*8 + round(e[5])*4 + round(e[6])*2 + round(e[7])))]
                entry['char'] = int(round(e[8])*128 + round(e[9])*64 + round(e[10])*32 + \
                                round(e[11])*16 + round(e[12])*8 + round(e[13])*4 + \
                                round(e[14])*2 + round(e[15]))
                ret.append(entry)

        return ret


